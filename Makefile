GO ?= go

.PHONY: fmt
fmt:
	$(GO) fmt ./...

.PHONY: bench
bench:
	$(GO) test -benchmem -bench=. > results.txt
