package encoding_bench

import (
	"bytes"
	"encoding/asn1"
	"encoding/gob"
	"encoding/json"
	"encoding/xml"
	"io/ioutil"
	"os"
	"testing"

	jsoniter "github.com/json-iterator/go"
	"go.jolheiser.com/randomuser"
)

var user randomuser.User

func TestMain(m *testing.M) {

	userContent, err := ioutil.ReadFile("user.json")
	if err != nil {
		panic(err)
	}

	if err := json.Unmarshal(userContent, &user); err != nil {
		panic(err)
	}

	os.Exit(m.Run())
}

func BenchmarkASN1(b *testing.B) {
	m, err := asn1.Marshal(user)
	if err != nil {
		b.Logf("could not marshal user: %v", err)
		b.FailNow()
	}
	b.Run("Marshal", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			if _, err := asn1.Marshal(user); err != nil {
				b.Logf("could not marshal: %v", err)
				b.Fail()
			}
		}
	})
	b.Run("Unmarshal", func(b *testing.B) {
		var discard randomuser.User
		for i := 0; i < b.N; i++ {
			if _, err := asn1.Unmarshal(m, &discard); err != nil {
				b.Logf("could not unmarshal: %v", err)
				b.Fail()
			}
		}
	})
}

func BenchmarkGOB(b *testing.B) {
	var encoding bytes.Buffer
	enc := gob.NewEncoder(&encoding)
	b.Run("Encode", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			if err := enc.Encode(user); err != nil {
				b.Logf("could not encode: %v", err)
				b.Fail()
			}
			encoding.Reset()
		}
	})

	var decoding bytes.Buffer
	enc = gob.NewEncoder(&decoding)
	dec := gob.NewDecoder(&decoding)
	b.Run("Decode", func(b *testing.B) {
		var discard randomuser.User
		for i := 0; i < b.N; i++ {
			b.StopTimer()
			if err := enc.Encode(user); err != nil {
				b.Logf("could not encode: %v", err)
				b.Fail()
			}
			b.StartTimer()
			if err := dec.Decode(&discard); err != nil {
				b.Logf("could not decode: %v", err)
				b.Fail()
			}
		}
	})
}

func BenchmarkJSON(b *testing.B) {
	m, err := json.Marshal(user)
	if err != nil {
		b.Logf("could not marshal user: %v", err)
		b.FailNow()
	}
	b.Run("Marshal", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			if _, err := json.Marshal(user); err != nil {
				b.Logf("could not marshal: %v", err)
				b.Fail()
			}
		}
	})
	b.Run("Unmarshal", func(b *testing.B) {
		var discard randomuser.User
		for i := 0; i < b.N; i++ {
			if err := json.Unmarshal(m, &discard); err != nil {
				b.Logf("could not unmarshal: %v", err)
				b.Fail()
			}
		}
	})
}

func BenchmarkJSONiter(b *testing.B) {
	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	m, err := json.Marshal(user)
	if err != nil {
		b.Logf("could not marshal user: %v", err)
		b.FailNow()
	}
	b.Run("Marshal", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			if _, err := json.Marshal(user); err != nil {
				b.Logf("could not marshal: %v", err)
				b.Fail()
			}
		}
	})
	b.Run("Unmarshal", func(b *testing.B) {
		var discard randomuser.User
		for i := 0; i < b.N; i++ {
			if err := json.Unmarshal(m, &discard); err != nil {
				b.Logf("could not unmarshal: %v", err)
				b.Fail()
			}
		}
	})
}

func BenchmarkXML(b *testing.B) {
	m, err := xml.Marshal(user)
	if err != nil {
		b.Logf("could not marshal user: %v", err)
		b.FailNow()
	}
	b.Run("Marshal", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			if _, err := xml.Marshal(user); err != nil {
				b.Logf("could not marshal: %v", err)
				b.Fail()
			}
		}
	})
	b.Run("Unmarshal", func(b *testing.B) {
		var discard randomuser.User
		for i := 0; i < b.N; i++ {
			if err := xml.Unmarshal(m, &discard); err != nil {
				b.Logf("could not unmarshal: %v", err)
				b.Fail()
			}
		}
	})
}
