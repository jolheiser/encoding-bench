//+build ignore

package main

import (
	"encoding/json"
	"os"

	"go.jolheiser.com/randomuser"
)

func main() {
	client := randomuser.New()

	resp, err := client.Query(randomuser.RequestOptions{
		Results: 1,
	})
	if err != nil {
		panic(err)
	}

	user := resp.Results[0]
	out, err := json.MarshalIndent(user, "", "\t")
	if err != nil {
		panic(err)
	}

	fi, err := os.Create("user.json")
	if err != nil {
		panic(err)
	}
	defer fi.Close()

	if _, err := fi.Write(out); err != nil {
		panic(err)
	}
}
