module gitea.com/jolheiser/encoding-bench

go 1.15

require (
	github.com/json-iterator/go v1.1.10
	go.jolheiser.com/randomuser v0.0.1
)
